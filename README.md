- Requerimentos:
    + .NET Core 3.1
    + SQLServer

- Instalação:
    + Criar banco chamado adiq e configurar o arquivo appSettings.json
    + Rodar as migrations

- Usuário:
    + Existe um usuário criado com email "matheuscruz@ioasys.com.br" e senha "senha123"

- Swagger
    + Swagger está instalado na url /api-docs (provavelmente na url http://localhost:5000/api-docs)

- OBS:
    + O frontend não foi feito, apenas o projeto web com as applications e cosumo, não foi feito a parte de tela