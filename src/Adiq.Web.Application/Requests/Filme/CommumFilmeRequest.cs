﻿namespace Adiq.Web.Application.Requests.Filme
{
    public class CommumFilmeRequest
    {
        public string Nome { get; set; }
        public string Descricao { get; set; }
        public string Diretor { get; set; }
        public decimal Media { get; set; }
        public string Genero { get; set; }
    }
}