﻿using System;

namespace Adiq.Web.Application.Requests.Filme
{
    public class UpdateFilmeRequest : CommumFilmeRequest
    {
        public Guid Id { get; set; }
    }
}