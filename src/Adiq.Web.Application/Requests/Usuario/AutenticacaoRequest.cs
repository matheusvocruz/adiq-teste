﻿namespace Adiq.Web.Application.Requests.Usuario
{
    public class AutenticacaoRequest
    {
        public string Email { get; set; }
        public string Senha { get; set; }
    }
}
