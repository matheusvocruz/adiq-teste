﻿using Adiq.Web.Application.Requests.Filme;
using Adiq.Web.Application.Responses;
using Adiq.Web.Application.Responses.Filme;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Adiq.Web.Application.Interfaces
{
    public interface IFilmeRepository
    {
        Task<List<FilmeResponse>> GetAll();
        Task<FilmeResponse> GetById(Guid id);
        Task<ResponseResult> Create(CreateFilmeRequest request);
        Task<ResponseResult> Update(UpdateFilmeRequest request);
        Task<ResponseResult> Delete(Guid id);
        void JwtBearer(string jwtToken);
    }
}
