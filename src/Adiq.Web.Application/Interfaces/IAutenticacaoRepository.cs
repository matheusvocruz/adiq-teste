﻿using Adiq.Web.Application.Requests.Usuario;
using Adiq.Web.Application.Responses.Usuario;
using System.Threading.Tasks;

namespace Adiq.Web.Application.Interfaces
{
    public interface IAutenticacaoRepository
    {
        Task<TokenResponse> Autenticar(AutenticacaoRequest request);
    }
}
