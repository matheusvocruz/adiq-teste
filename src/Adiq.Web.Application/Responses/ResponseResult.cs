﻿using System.Collections.Generic;

namespace Adiq.Web.Application.Responses
{
    public class ResponseResult
    {
        public ResponseResult() { }

        public string Title { get; set; }
        public int Status { get; set; }
        public ResponseErrorMessages Errors { get; set; }
    }

    public class ResponseErrorMessages
    {
        public ResponseErrorMessages() { }

        public List<string> Messages { get; set; }
    }
}
