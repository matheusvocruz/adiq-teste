﻿using Adiq.Web.Application.Interfaces;
using Adiq.Web.Application.Requests.Usuario;
using Adiq.Web.Application.Responses.Usuario;
using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace Adiq.Web.Application.Repositories
{
    public class AutenticacaoRepository : IAutenticacaoRepository
    {
        private readonly HttpClient _httpClient;

        public AutenticacaoRepository(HttpClient httpClient)
        {
            httpClient.BaseAddress = new Uri("http://localhost:5000/api/");
            _httpClient = httpClient;
        }

        public async Task<TokenResponse> Autenticar(AutenticacaoRequest request)
        {
            var response = await _httpClient.PostAsync(
               "v1/autenticacao",
               this.PrepareRequest(request)
            );

            var result = JsonConvert.DeserializeObject<TokenResponse>(response.Content.ReadAsStringAsync().Result);

            return result;
        }

        private ByteArrayContent PrepareRequest(object request)
        {
            var myContent = JsonConvert.SerializeObject(request);
            var buffer = System.Text.Encoding.UTF8.GetBytes(myContent);
            var byteContent = new ByteArrayContent(buffer);
            byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

            return byteContent;
        }
    }
}
