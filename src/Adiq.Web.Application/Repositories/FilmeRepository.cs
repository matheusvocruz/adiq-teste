﻿using Adiq.Web.Application.Interfaces;
using Adiq.Web.Application.Requests.Filme;
using Adiq.Web.Application.Responses;
using Adiq.Web.Application.Responses.Filme;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace Adiq.Web.Application.Repositories
{
    public class FilmeRepository : IFilmeRepository
    {
        private readonly HttpClient _httpClient;

        public FilmeRepository(HttpClient httpClient)
        {
            httpClient.BaseAddress = new Uri("http://localhost:5000/api/");
            _httpClient = httpClient;
        }

        public async Task<List<FilmeResponse>> GetAll()
        {
            var response = await _httpClient.GetAsync("v1/filme");

            var result = JsonConvert.DeserializeObject<List<FilmeResponse>>(response.Content.ReadAsStringAsync().Result);

            return result;
        }

        public async Task<FilmeResponse> GetById(Guid id)
        {
            var response = await _httpClient.GetAsync($"v1/filme/{id}");

            var result = JsonConvert.DeserializeObject<FilmeResponse>(response.Content.ReadAsStringAsync().Result);

            return result;
        }

        public async Task<ResponseResult> Create(CreateFilmeRequest request)
        {
            var response = await _httpClient.PostAsync(
               "v1/filme",
               PrepareRequest(request));

            return JsonConvert.DeserializeObject<ResponseResult>(response.Content.ReadAsStringAsync().Result);
        }

        public async Task<ResponseResult> Update(UpdateFilmeRequest request)
        {
            var response = await _httpClient.PutAsync(
               $"v1/filme/{request.Id}",
               PrepareRequest(request));

            return JsonConvert.DeserializeObject<ResponseResult>(response.Content.ReadAsStringAsync().Result);
        }

        public async Task<ResponseResult> Delete(Guid id)
        {
            var response = await _httpClient.DeleteAsync($"v1/filme/{id}");

            return JsonConvert.DeserializeObject<ResponseResult>(response.Content.ReadAsStringAsync().Result);
        }

        private ByteArrayContent PrepareRequest(object request)
        {
            var myContent = JsonConvert.SerializeObject(request);
            var buffer = System.Text.Encoding.UTF8.GetBytes(myContent);
            var byteContent = new ByteArrayContent(buffer);
            byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

            return byteContent;
        }

        public void JwtBearer(string jwtToken)
        {
            _httpClient.DefaultRequestHeaders.Add("Authorization", "Bearer " + jwtToken);
        }
    }
}
