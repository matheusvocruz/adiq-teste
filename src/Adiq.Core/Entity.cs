﻿using FluentValidation.Results;
using System;

namespace Adiq.Core
{
    public abstract class Entity
    {
        protected Entity()
        {

        }

        public Guid Id { get; set; }
        public ValidationResult ValidationResult { get; protected set; }
    }
}
