﻿using Adiq.Core.Interfaces;
using FluentValidation.Results;
using MediatR;
using System.Threading.Tasks;

namespace Adiq.Core
{
    public class MediatorHandler : IMediatorHandler
    {
        private readonly IMediator _mediator;

        public MediatorHandler(IMediator mediator)
        {
            _mediator = mediator;
        }

        public async Task<ValidationResult> SendCommand<T>(T command) where T : Command
        {
            return await _mediator.Send(command);
        }
    }
}
