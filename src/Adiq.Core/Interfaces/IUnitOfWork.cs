﻿using System.Threading.Tasks;

namespace Adiq.Core.Interfaces
{
    public interface IUnitOfWork
    {
        Task<bool> Commit();
    }
}
