﻿using Adiq.Application.Requests.Filme;
using Adiq.Application.Responses.Filme;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Adiq.API.Tests.Filme
{
    public class FilmesTests : IClassFixture<CustomWebApplicationFactory<Startup>>
    {
        public HttpClient Client { get; }
        private readonly int version = 1;

        public FilmesTests(CustomWebApplicationFactory<Startup> factory)
        {
            Client = factory.CreateClient();
            Client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", "teste");
        }

        [Fact(DisplayName = "Buscar todos os filmes (sucesso)")]
        public async Task GetFilmes__Success()
        {
            var result = await GetFilmes();
            Assert.Equal(HttpStatusCode.OK, result.StatusCode);

            var filmes = JsonConvert.DeserializeObject<List<FilmeResponse>>(result.Content.ReadAsStringAsync().Result);

            Assert.NotEmpty(filmes);
        }

        private Task<HttpResponseMessage> GetFilmes()
        {
            return Client.GetAsync($"/api/v{version}/filme");
        }

        [Fact(DisplayName = "Buscar filme pelo id (sucesso)")]
        public async Task GetFilmesById__Success()
        {
            var result = await GetFilme(Guid.Parse("6DE16035-796D-4FFB-91E7-D9A076ECA147"));
            Assert.Equal(HttpStatusCode.OK, result.StatusCode);

            var filme = JsonConvert.DeserializeObject<FilmeResponse>(result.Content.ReadAsStringAsync().Result);

            Assert.True(filme.Id != Guid.Empty);
        }

        [Fact(DisplayName = "Buscar filme pelo id (sucesso)")]
        public async Task GetFilmesById__Error_NotFound()
        {
            var guid = Guid.NewGuid();
            var result = await GetFilme(guid);
            Assert.Equal(HttpStatusCode.NoContent, result.StatusCode);

            var filme = JsonConvert.DeserializeObject<FilmeResponse>(result.Content.ReadAsStringAsync().Result);

            Assert.Null(filme);
        }

        private Task<HttpResponseMessage> GetFilme(Guid id)
        {
            return Client.GetAsync($"/api/v{version}/filme/{id}");
        }

        [Fact(DisplayName = "Cadastrar Filme (sucesso)")]
        public async Task CreateFilme__Success()
        {
            var result = await Post(new CreateFilmeRequest
            {
                Nome = "Shadow in the Cloud (2021)",
                Descricao = "A female WWII pilot traveling with top secret documents on a B-17 Flying Fortress",
                Diretor = "Roseanne Liang",
                Media = 5,
                Genero = "Action, Horror, War"
            });
            Assert.Equal(HttpStatusCode.NoContent, result.StatusCode);
        }

        [Fact(DisplayName = "Cadastrar Filme com o mesmo nome")]
        public async Task CreateFilme__Error_Same_Name()
        {
            var result = await Post(new CreateFilmeRequest
            {
                Nome = "Shadow in the Cloud (2021)",
                Descricao = "A female WWII pilot traveling with top secret documents on a B-17 Flying Fortress",
                Diretor = "Roseanne Liang",
                Media = 5,
                Genero = "Action, Horror, War"
            });
            Assert.Equal(HttpStatusCode.BadRequest, result.StatusCode);
        }

        private Task<HttpResponseMessage> Post(CreateFilmeRequest request)
        {
            string registerData = JsonConvert.SerializeObject(request);
            StringContent content = new StringContent(registerData, Encoding.UTF8, "application/json");
            return Client.PostAsync($"/api/v{version}/filme", content);
        }

        [Fact(DisplayName = "Editar Filme (sucesso)")]
        public async Task UpdateFilme__Success()
        {
            var result = await Put(new UpdateFilmeRequest
            {
                Id = Guid.Parse("6DE16035-796D-4FFB-91E7-D9A076ECA147"),
                Nome = "Shadow in the Cloud (2021-UPDATED)",
                Descricao = "A female WWII pilot traveling with top secret documents on a B-17 Flying Fortress",
                Diretor = "Roseanne Liang",
                Media = 5,
                Genero = "Action, Horror, War"
            });
            Assert.Equal(HttpStatusCode.NoContent, result.StatusCode);
        }

        [Fact(DisplayName = "Editar Filme que não existe")]
        public async Task UpdateFilme__Error_NotFound()
        {
            var result = await Put(new UpdateFilmeRequest
            {
                Id = Guid.NewGuid(),
                Nome = "Shadow in the Cloud (2021)",
                Descricao = "A female WWII pilot traveling with top secret documents on a B-17 Flying Fortress",
                Diretor = "Roseanne Liang",
                Media = 5,
                Genero = "Action, Horror, War"
            });
            Assert.Equal(HttpStatusCode.BadRequest, result.StatusCode);
        }

        private Task<HttpResponseMessage> Put(UpdateFilmeRequest request)
        {
            string registerData = JsonConvert.SerializeObject(request);
            StringContent content = new StringContent(registerData, Encoding.UTF8, "application/json");
            return Client.PutAsync($"/api/v{version}/filme/{request.Id}", content);
        }

        [Fact(DisplayName = "Deletar Filme (sucesso)")]
        public async Task DeleteFilme__Success()
        {
            var result = await Delete(Guid.Parse("6DE16035-796D-4FFB-91E7-D9A076ECA150"));
            Assert.Equal(HttpStatusCode.NoContent, result.StatusCode);
        }

        [Fact(DisplayName = "Excluir Filme que não existe")]
        public async Task DeteleVeiculo__Error_NotFound()
        {
            var result = await Delete(Guid.NewGuid());
            Assert.Equal(HttpStatusCode.BadRequest, result.StatusCode);
        }

        private Task<HttpResponseMessage> Delete(Guid id)
        {
            return Client.DeleteAsync($"/api/v{version}/filme/{id}");
        }
    }

}
