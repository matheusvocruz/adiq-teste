﻿using Adiq.Application.Requests.Usuario;
using Adiq.Application.Responses.Usuario;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Xunit;


namespace Adiq.API.Tests.Autenticacao
{
    public class AutenticacaoTests : IClassFixture<CustomWebApplicationFactory<Startup>>
    {
        public HttpClient Client { get; }
        private readonly int version = 1;

        public AutenticacaoTests(CustomWebApplicationFactory<Startup> factory)
        {
            Client = factory.CreateClient();
            Client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", "teste");
        }

        [Fact(DisplayName = "Autenticação (sucesso)")]
        public async Task Autenticacao__Success()
        {
            var result = await Post(new AutenticationUsuarioRequest
            {
                Email =  "matheuscruz@ioasys.com.br",
                Senha = "senha123"
            });

            Assert.Equal(HttpStatusCode.OK, result.StatusCode);
            
            var token = JsonConvert.DeserializeObject<TokenResponse>(result.Content.ReadAsStringAsync().Result);

            Assert.True(token.Token != string.Empty);
            Assert.True(token.Expiration != null);
        }

        [Fact(DisplayName = "Autenticação (erro)")]
        public async Task Autenticacao__Error_Not_Found()
        {
            var result = await Post(new AutenticationUsuarioRequest
            {
                Email = "matheuscruz@ioasys.com.br",
                Senha = "senha1234"
            });

            Assert.Equal(HttpStatusCode.NoContent, result.StatusCode);
        }

        private Task<HttpResponseMessage> Post(AutenticationUsuarioRequest request)
        {
            string registerData = JsonConvert.SerializeObject(request);
            StringContent content = new StringContent(registerData, Encoding.UTF8, "application/json");
            return Client.PostAsync($"/api/v{version}/autenticacao", content);
        }
    }
}
