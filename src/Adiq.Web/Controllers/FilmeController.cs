﻿using Adiq.Web.Application.Interfaces;
using Adiq.Web.Application.Requests.Filme;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace Adiq.Web.Controllers
{
    [Route("filme")]
    public class FilmeController : Controller
    {
        private readonly IFilmeRepository _filmeRepository;

        public FilmeController(IFilmeRepository filmeRepository)
        {
            _filmeRepository = filmeRepository;
        }

        [HttpGet]
        public async Task<JsonResult> Get()
            => Json(await _filmeRepository.GetAll());

        [HttpGet("{id}")]
        public async Task<JsonResult> GetById([FromRoute] Guid id)
            => Json(await _filmeRepository.GetById(id));

        [HttpPost]
        public async Task<JsonResult> Post([FromBody] CreateFilmeRequest request)
            => Json(await _filmeRepository.Create(request));

        [HttpPut("{id}")]
        public async Task<JsonResult> Put([FromRoute] Guid id, [FromBody] UpdateFilmeRequest request)
        {
            request.Id = id;

            return Json(await _filmeRepository.Update(request));
        }

        [HttpDelete("{id}")]
        public async Task<JsonResult> Delete([FromRoute] Guid id)
            => Json(await _filmeRepository.Delete(id));
    }
}
