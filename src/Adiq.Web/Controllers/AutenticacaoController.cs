﻿using Adiq.Web.Application.Interfaces;
using Adiq.Web.Application.Requests.Usuario;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Adiq.Web.Controllers
{
    public class AutenticacaoController : Controller
    {
        private readonly IAutenticacaoRepository _autenticacaoRepository;

        public AutenticacaoController(IAutenticacaoRepository autenticacaoRepository)
        {
            _autenticacaoRepository = autenticacaoRepository;
        }

        [HttpPost]
        public async Task<JsonResult> Index([FromBody] AutenticacaoRequest request)
            => Json(await _autenticacaoRepository.Autenticar(request));
    }
}
