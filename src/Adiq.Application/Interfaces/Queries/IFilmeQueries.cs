﻿using Adiq.Application.Requests;
using Adiq.Application.Responses.Filme;
using Adiq.Core;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Adiq.Application.Interfaces.Queries
{
    public interface IFilmeQueries
    {
        Task<FilmeResponse> RetornarFilmePeloId(Guid id);
        Task<List<FilmeResponse>> RetornarFilmes();
    }
}
