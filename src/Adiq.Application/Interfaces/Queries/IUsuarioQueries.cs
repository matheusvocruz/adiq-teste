﻿using Adiq.Application.Requests.Usuario;
using Adiq.Application.Responses.Usuario;
using System;
using System.Threading.Tasks;

namespace Adiq.Application.Interfaces.Queries
{
    public interface IUsuarioQueries
    {
        Task<TokenResponse> Autenticar(AutenticationUsuarioRequest request);
        Task<TokenResponse> RefreshToken(Guid guid);
        Task<string> SenhaCriptografada(string senha);
    }
}
