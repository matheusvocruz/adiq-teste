﻿using Adiq.Application.Validators.Filme;
using Adiq.Core;
using System;

namespace Adiq.Application.Requests.Filme
{
    public class UpdateFilmeRequest : Command
    {
        public Guid Id { get; set; }
        public string Nome { get; set; }
        public string Descricao { get; set; }
        public string Diretor { get; set; }
        public decimal Media { get; set; }
        public string Genero { get; set; }

        public override bool IsValid()
        {
            ValidationResult = new UpdateFilmeValidation().Validate(this);
            return ValidationResult.IsValid;
        }
    }
}
