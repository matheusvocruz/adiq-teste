﻿using Adiq.Application.Validators.Filme;
using Adiq.Core;
using System;

namespace Adiq.Application.Requests.Filme
{
    public class DeleteFilmeRequest : Command
    {
        public Guid Id { get; set; }

        public override bool IsValid()
        {
            ValidationResult = new DeleteFilmeValidation().Validate(this);
            return ValidationResult.IsValid;
        }
    }
}
