﻿using Adiq.Core;

namespace Adiq.Application.Requests
{
    public class ListFilmeRequest
    {
        public string Nome { get; set; }
        public string Diretor { get; set; }
        public string Genero { get; set; }
    }
}
