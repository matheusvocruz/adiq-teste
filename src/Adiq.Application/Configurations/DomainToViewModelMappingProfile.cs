﻿using Adiq.Application.Requests;
using Adiq.Application.Responses.Filme;
using Adiq.Application.Responses.Usuario;
using Adiq.Core;
using Adiq.Data.Entities;
using AutoMapper;
using System.Collections.Generic;

namespace Adiq.Application.Configurations
{
    public class DomainToViewModelMappingProfile : Profile
    {
        public DomainToViewModelMappingProfile()
        {
            CreateMap<Filme, FilmeResponse>();
            CreateMap<Data.Requests.Filme.ListFilmeRequest, ListFilmeRequest>();
            CreateMap<Data.ValueObjetcs.TokenResponse, TokenResponse>();
        }
    }
}
