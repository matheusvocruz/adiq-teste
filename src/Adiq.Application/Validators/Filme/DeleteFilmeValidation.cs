﻿using Adiq.Application.Requests.Filme;
using FluentValidation;

namespace Adiq.Application.Validators.Filme
{
    public class DeleteFilmeValidation : AbstractValidator<DeleteFilmeRequest>
    {
        public DeleteFilmeValidation()
        {
            RuleFor(c => c.Id)
                .NotEmpty();
        }
    }
}
