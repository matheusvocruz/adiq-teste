﻿using Adiq.Application.Requests.Filme;
using FluentValidation;

namespace Adiq.Application.Validators.Filme
{
    public class UpdateFilmeValidation : AbstractValidator<UpdateFilmeRequest>
    {
        public UpdateFilmeValidation()
        {
            RuleFor(c => c.Id)
                .NotEmpty();

            RuleFor(c => c.Nome)
                .NotEmpty();

            RuleFor(c => c.Descricao)
                .NotEmpty();

            RuleFor(c => c.Diretor)
                .NotEmpty();

            RuleFor(c => c.Media)
                .NotEmpty()
                .GreaterThanOrEqualTo(0)
                .LessThanOrEqualTo(5);

            RuleFor(c => c.Genero)
                .NotEmpty();
        }
    }
}
