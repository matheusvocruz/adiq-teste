﻿using Adiq.Application.Requests.Filme;
using FluentValidation;

namespace Adiq.Application.Validators.Filme
{
    public class CreateFilmeValidation : AbstractValidator<CreateFilmeRequest>
    {
        public CreateFilmeValidation()
        {
            RuleFor(c => c.Nome)
                .NotEmpty()
                .MaximumLength(100);

            RuleFor(c => c.Descricao)
                .NotEmpty()
                .MaximumLength(100);

            RuleFor(c => c.Diretor)
                .NotEmpty()
                .MaximumLength(100);

            RuleFor(c => c.Media)
                .NotEmpty()
                .GreaterThanOrEqualTo(0)
                .LessThanOrEqualTo(5);

            RuleFor(c => c.Genero)
                .NotEmpty()
                .MaximumLength(100);
        }
    }
}
