﻿using AutoMapper;
using Adiq.Application.Interfaces.Queries;
using Adiq.Application.Requests.Usuario;
using Adiq.Application.Responses.Usuario;
using System.Threading.Tasks;
using IMDb.Data.Interfaces.Repositorios;
using System;

namespace Adiq.Application.Queries
{
    public class UsuarioQueries : IUsuarioQueries
    {
        private readonly IMapper _mapper;
        private readonly IUsuarioRepository _usuarioRepository;

        public UsuarioQueries(
            IMapper mapper,
            IUsuarioRepository usuarioRepository
        )
        {
            _mapper = mapper;
            _usuarioRepository = usuarioRepository;
        }

        public async Task<TokenResponse> Autenticar(AutenticationUsuarioRequest request)
        {
            return _mapper.Map<TokenResponse>(await _usuarioRepository.Autenticar(request.Email, request.Senha));
        }

        public async Task<TokenResponse> RefreshToken(Guid id)
        {
            return _mapper.Map<TokenResponse>(await _usuarioRepository.RefreshToken(id));
        }

        public async Task<string> SenhaCriptografada(string senha)
        {
            return _mapper.Map<string>(_usuarioRepository.HashSenha(senha));
        }
    }
}
