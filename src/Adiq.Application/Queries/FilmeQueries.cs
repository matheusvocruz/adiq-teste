﻿using Adiq.Application.Requests;
using Adiq.Application.Responses.Filme;
using Adiq.Core;
using Adiq.Data.Interfaces.Repositories;
using AutoMapper;
using Adiq.Application.Interfaces.Queries;
using System.Threading.Tasks;
using System.Collections.Generic;
using System;

namespace Adiq.Application.Queries
{
    public class FilmeQueries : IFilmeQueries
    {
        private readonly IMapper _mapper;
        private readonly IFilmeRepository _filmeRepository;

        public FilmeQueries(
            IMapper mapper,
            IFilmeRepository filmeRepository
        )
        {
            _mapper = mapper;
            _filmeRepository = filmeRepository;
        }

        public async Task<FilmeResponse> RetornarFilmePeloId(Guid id)
        {
            return _mapper.Map<FilmeResponse>(await _filmeRepository.RetornarFilmePeloId(id));
        }

        public async Task<List<FilmeResponse>> RetornarFilmes()
        {
            return _mapper.Map<List<FilmeResponse>>(await _filmeRepository.RetornarFilmes());
        }
    }
}
