﻿using FluentValidation.Results;
using Adiq.Application.Requests.Filme;
using Adiq.Core;
using MediatR;
using System.Threading;
using System.Threading.Tasks;
using Adiq.Data.Interfaces.Repositories;
using System;

namespace Adiq.Application.Commands.Filme
{
    public class CreateFilmeCommandHandler :
        CommandHandler, IRequestHandler<CreateFilmeRequest, ValidationResult>
    {

        private readonly IFilmeRepository _filmeRepository;

        public CreateFilmeCommandHandler(IFilmeRepository filmeRepository)
        {
            _filmeRepository = filmeRepository;
        }

        public async Task<ValidationResult> Handle(CreateFilmeRequest request, CancellationToken cancellationToken)
        {
            if (!request.IsValid()) return request.ValidationResult;

            var verificaDuplicidade = await _filmeRepository.RetornarFilmePeloNome(request.Nome);

            if (verificaDuplicidade != null)
            {
                AddError("Já existe um filme com esse nome");
                return ValidationResult;
            }

            var filme = new Data.Entities.Filme(Guid.NewGuid(), request.Nome, request.Descricao, request.Diretor, request.Media, request.Genero);

            _filmeRepository.Create(filme);

            return await PersistData(_filmeRepository.UnitOfWork);
        }
    }
}
