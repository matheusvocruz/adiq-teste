﻿using FluentValidation.Results;
using Adiq.Application.Requests.Filme;
using Adiq.Core;
using MediatR;
using System.Threading;
using System.Threading.Tasks;
using Adiq.Data.Interfaces.Repositories;

namespace Adiq.Application.Commands.Filme
{
    public class UpdateFilmeCommandHandler :
        CommandHandler, IRequestHandler<UpdateFilmeRequest, ValidationResult>
    {

        private readonly IFilmeRepository _filmeRepository;

        public UpdateFilmeCommandHandler(IFilmeRepository filmeRepository)
        {
            _filmeRepository = filmeRepository;
        }

        public async Task<ValidationResult> Handle(UpdateFilmeRequest request, CancellationToken cancellationToken)
        {
            if (!request.IsValid()) return request.ValidationResult;

            var filme = await _filmeRepository.RetornarFilmePeloId(request.Id);

            if (filme == null)
            {
                AddError("Esse filme não existe");
                return ValidationResult;
            }

            var verificaDuplicidade = await _filmeRepository.RetornarFilmePeloNome(request.Nome, request.Id);

            if (verificaDuplicidade != null)
            {
                AddError("Já existe um filme com esse nome");
                return ValidationResult;
            }

            filme.Update(request.Nome, request.Descricao, request.Diretor, request.Media, request.Genero);

            _filmeRepository.Update(filme);

            return await PersistData(_filmeRepository.UnitOfWork);
        }
    }
}
