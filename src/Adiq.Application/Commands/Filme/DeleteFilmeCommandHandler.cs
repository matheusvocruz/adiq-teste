﻿using FluentValidation.Results;
using Adiq.Application.Requests.Filme;
using Adiq.Core;
using MediatR;
using System.Threading;
using System.Threading.Tasks;
using Adiq.Data.Interfaces.Repositories;

namespace Adiq.Application.Commands.Filme
{
    public class DeleteFilmeCommandHandler :
        CommandHandler, IRequestHandler<DeleteFilmeRequest, ValidationResult>
    {

        private readonly IFilmeRepository _filmeRepository;

        public DeleteFilmeCommandHandler(IFilmeRepository filmeRepository)
        {
            _filmeRepository = filmeRepository;
        }

        public async Task<ValidationResult> Handle(DeleteFilmeRequest request, CancellationToken cancellationToken)
        {
            if (!request.IsValid()) return request.ValidationResult;

            var filme = await _filmeRepository.RetornarFilmePeloId(request.Id);

            if (filme == null)
            {
                AddError("Esse filme não existe");
                return ValidationResult;
            }

            _filmeRepository.Delete(filme);

            return await PersistData(_filmeRepository.UnitOfWork);
        }
    }
}
