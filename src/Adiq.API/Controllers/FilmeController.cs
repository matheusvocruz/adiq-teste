﻿using Adiq.Application.Interfaces.Queries;
using Adiq.Application.Requests;
using Adiq.Application.Requests.Filme;
using Adiq.Application.Responses.Filme;
using Adiq.Core;
using Adiq.Core.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Adiq.API.Controllers
{
    [Authorize]
    [ApiVersion("1.0")]
    public class FilmeController : MainController
    {
        private readonly IFilmeQueries _iFilmeQueries;
        private readonly IMediatorHandler _mediatorHandler;

        public FilmeController(
            IFilmeQueries iFilmeQueries,
            IMediatorHandler mediatorHandler
        )
        {
            _iFilmeQueries = iFilmeQueries;
            _mediatorHandler = mediatorHandler;
        }

        /// <summary>
        /// Listagem dos Filmes
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(200, Type = typeof(List<FilmeResponse>))]
        public async Task<IActionResult> IndexAsync()
            => CustomResponse(await _iFilmeQueries.RetornarFilmes());

        /// <summary>
        /// Retornar Filme
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id}")]
        [ProducesResponseType(200, Type = typeof(FilmeResponse))]
        public async Task<IActionResult> IndexAsync([FromRoute] Guid id)
            => CustomResponse(await _iFilmeQueries.RetornarFilmePeloId(id));

        /// <summary>
        /// Criação do Filme
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(200, Type = typeof(string))]
        public async Task<IActionResult> CreateAsync([FromBody] CreateFilmeRequest request)
            => CustomResponse(await _mediatorHandler.SendCommand(request));

        /// <summary>
        /// Atualização do Filme
        /// </summary>
        /// <returns></returns>
        [HttpPut("{id}")]
        [ProducesResponseType(200, Type = typeof(string))]
        public async Task<IActionResult> UpdateAsync([FromRoute] Guid id, [FromBody] UpdateFilmeRequest request)
        {
            request.Id = id;
            return CustomResponse(await _mediatorHandler.SendCommand(request));
        }

        /// <summary>
        /// Remoção do Filme
        /// </summary>
        /// <returns></returns>
        [HttpDelete("{id}")]
        [ProducesResponseType(200, Type = typeof(string))]
        public async Task<IActionResult> DeleteAsync([FromRoute] Guid id)
            => CustomResponse(await _mediatorHandler.SendCommand(new DeleteFilmeRequest { Id = id }));
    }
}
