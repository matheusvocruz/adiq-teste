﻿using Microsoft.Extensions.DependencyInjection;
using MediatR;
using Adiq.Core.Interfaces;
using Adiq.Core;
using FluentValidation.Results;
using IMDb.Data.Repositorios;
using Adiq.Application.Queries;
using AutoMapper;
using Adiq.Application.Configurations;
using Adiq.Data.Contexts;
using Adiq.Application.Commands.Filme;
using Adiq.Application.Requests.Filme;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.Extensions.DependencyInjection.Extensions;
using IMDb.Data.Interfaces.Repositorios;
using Adiq.Data.Interfaces.Repositories;
using Adiq.Application.Interfaces.Queries;

namespace Adiq.API.Configuration
{
    public static class DependencyInjectionConfig
    {
        public static void RegisterServices(this IServiceCollection services)
        {
            //Mediator
            services.AddScoped<IMediatorHandler, MediatorHandler>();
            //Mediator - Filme
            services.AddScoped<IRequestHandler<CreateFilmeRequest, ValidationResult>, CreateFilmeCommandHandler>();
            services.AddScoped<IRequestHandler<UpdateFilmeRequest, ValidationResult>, UpdateFilmeCommandHandler>();
            services.AddScoped<IRequestHandler<DeleteFilmeRequest, ValidationResult>, DeleteFilmeCommandHandler>();


            //Queries
            services.AddScoped<IUsuarioQueries, UsuarioQueries>();
            services.AddScoped<IFilmeQueries, FilmeQueries>();


            //Repositórios
            services.AddScoped<IFilmeRepository, FilmeRepository>();
            services.AddScoped<IUsuarioRepository, UsuarioRepository>();


            //Contextos
            services.AddScoped<AdiqContexto>();

            //
            services.AddHttpContextAccessor();
            services.TryAddSingleton<IActionContextAccessor, ActionContextAccessor>();


            //AutoMapper
            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new DomainToViewModelMappingProfile());
            });

            IMapper mapper = mappingConfig.CreateMapper();
            services.AddSingleton(mapper);
        }
    }
}
