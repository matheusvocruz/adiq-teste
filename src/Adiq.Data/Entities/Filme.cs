﻿using Adiq.Core;
using Adiq.Core.Interfaces;
using System;

namespace Adiq.Data.Entities
{
    public class Filme : Entity, IAggregateRoot
    {
        protected Filme() { }

        public Filme(
            Guid id,
            string nome,
            string descricao,
            string diretor,
            decimal media,
            string genero
        )
        {
            Id = id;
            Nome = nome;
            Descricao = descricao;
            Diretor = diretor;
            Media = media;
            Genero = genero;
        }

        public void Update(
            string nome,
            string descricao,
            string diretor,
            decimal media,
            string genero
        )
        {
            Nome = nome;
            Descricao = descricao;
            Diretor = diretor;
            Media = media;
            Genero = genero;
        }

        public string Nome { get; set; }
        public string Descricao { get; set; }
        public string Diretor { get; set; }
        public decimal Media { get; set; }
        public string Genero { get; set; }
    }
}
