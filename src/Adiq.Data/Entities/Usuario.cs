﻿using Adiq.Core;
using Adiq.Core.Interfaces;
using System;

namespace Adiq.Data.Entities
{
    public class Usuario : Entity, IAggregateRoot
    {
        protected Usuario()
        {

        }

        public Usuario(
            string nome,
            string email,
            string senha
        )
        {
            Nome = nome;
            Email = email;
            Senha = senha;
        }

        public string Nome { get; set; }
        public string Email { get; set; }
        public string Senha { get; set; }
    }
}
