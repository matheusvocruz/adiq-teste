﻿using Adiq.Core;
using Adiq.Core.Interfaces;
using Adiq.Data.Contexts;
using Adiq.Data.Entities;
using Adiq.Data.Interfaces.Repositories;
using Adiq.Data.Requests.Filme;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IMDb.Data.Repositorios
{
    public class FilmeRepository : IFilmeRepository
    {
        private readonly AdiqContexto _contexto;
        public IUnitOfWork UnitOfWork => _contexto;

        public FilmeRepository(AdiqContexto contexto)
        {
            _contexto = contexto;
        }

        public async Task<List<Filme>> RetornarFilmes()
        {
            return await _contexto.Filmes.AsNoTracking().ToListAsync();
        }

        public async Task<Filme> RetornarFilmePeloNome(string nome)
        {
            return await _contexto.Filmes.AsNoTracking().FirstOrDefaultAsync(x => x.Nome == nome);
        }

        public async Task<Filme> RetornarFilmePeloNome(string nome, Guid id)
        {
            return await _contexto.Filmes.AsNoTracking().FirstOrDefaultAsync(x => x.Nome == nome && x.Id != id);
        }

        public async Task<Filme> RetornarFilmePeloId(Guid id)
        {
            return await _contexto.Filmes.AsNoTracking().FirstOrDefaultAsync(x => x.Id == id);
        }

        public void Create(Filme filme)
        {
            _contexto.Filmes.Add(filme);
        }

        public void Update(Filme filme)
        {
            _contexto.Filmes.Update(filme);
        }

        public void Delete(Filme filme)
        {
            _contexto.Filmes.Remove(filme);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            _contexto.Dispose();
        }
    }
}
