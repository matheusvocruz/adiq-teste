﻿using System;

namespace Adiq.Data.ValueObjetcs
{
    public class TokenResponse
    {
        public string Token { get; set; }
        public DateTime Expiration { get; set; }
    }
}
