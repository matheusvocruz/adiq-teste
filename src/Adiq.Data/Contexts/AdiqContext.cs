﻿using FluentValidation.Results;
using Adiq.Core;
using Adiq.Core.Interfaces;
using Adiq.Data.Entities;
using Adiq.Data.Mapping;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace Adiq.Data.Contexts
{
    public class AdiqContexto : DbContext, IUnitOfWork
    {
        public AdiqContexto(DbContextOptions<AdiqContexto> options) : base(options) { }

        public DbSet<Filme> Filmes { get; set; }
        public DbSet<Usuario> Usuarios { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Ignore<ValidationResult>();

            foreach (var property in modelBuilder.Model.GetEntityTypes().SelectMany(e => e.GetProperties().Where(p => p.ClrType == typeof(string))))
            {
                property.SetColumnType("varchar(100)");
            }

            modelBuilder.ApplyConfigurationsFromAssembly(typeof(AdiqContexto).Assembly);

            foreach (var relationship in modelBuilder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
            {
                relationship.DeleteBehavior = DeleteBehavior.ClientSetNull;
            }

            foreach (var entityType in modelBuilder.Model.GetEntityTypes()
                .Where(t => typeof(Entity).IsAssignableFrom(t.ClrType)))
            {
                var entityTypeBuilder = modelBuilder.Entity(entityType.ClrType);
                entityTypeBuilder.Ignore("Guid");
            }

            modelBuilder.ApplyConfiguration(new FilmeMap());
        }

        public async Task<bool> Commit()
        {
            return await base.SaveChangesAsync() > 0;
        }
    }
}
