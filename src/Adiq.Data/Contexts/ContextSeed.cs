﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
//using POR.Desplugados.Domain.ValueObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Adiq.Data.Contexts
{
    public class ContextSeed
    {
        protected ContextSeed()
        {
        }

        public static async Task SeedAsync(AdiqContexto context,
            ILoggerFactory loggerFactory, int? retry = 0)
        {
            int retryForAvailability = retry.Value;
            try
            {
                await ConfigureFilmes(context);
            }
            catch (Exception ex)
            {
                if (retryForAvailability < 10)
                {
                    retryForAvailability++;
                    var log = loggerFactory.CreateLogger<ContextSeed>();
                    log.LogError(ex.Message);
                    await SeedAsync(context, loggerFactory, retryForAvailability);
                }
            }
        }

        private static async Task ConfigureFilmes(AdiqContexto context)
        {
            if (context.Filmes.Any())
                context.Filmes.RemoveRange(context.Filmes.AsNoTracking());

            context.Filmes.AddRange(
                new Entities.Filme(Guid.Parse("6de16035-796d-4ffb-91e7-d9a076eca147"), "Filme 1", "Descrição 1", "Diretor 1", 1E0m, "Genero 1"),
                new Entities.Filme(Guid.Parse("6de16035-796d-4ffb-91e7-d9a076eca148"), "Filme 2", "Descrição 2", "Diretor 2", 2E0m, "Genero 1"),
                new Entities.Filme(Guid.Parse("6de16035-796d-4ffb-91e7-d9a076eca149"), "Filme 3", "Descrição 3", "Diretor 1", 3E0m, "Genero 3"),
                new Entities.Filme(Guid.Parse("6de16035-796d-4ffb-91e7-d9a076eca150"), "Filme 4", "Descrição 4", "Diretor 2", 4E0m, "Genero 3")
            );

            await context.SaveChangesAsync();
        }
    }
}
