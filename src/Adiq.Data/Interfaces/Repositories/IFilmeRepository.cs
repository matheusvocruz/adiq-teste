﻿using Adiq.Core;
using Adiq.Core.Interfaces;
using Adiq.Data.Entities;
using Adiq.Data.Requests.Filme;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Adiq.Data.Interfaces.Repositories
{
    public interface IFilmeRepository : IRepository<Filme>
    {
        Task<List<Filme>> RetornarFilmes();
        Task<Filme> RetornarFilmePeloNome(string nome);
        Task<Filme> RetornarFilmePeloNome(string nome, Guid id);
        Task<Filme> RetornarFilmePeloId(Guid id);
        void Create(Filme filme);
        void Update(Filme filme);
        void Delete(Filme filme);
    }
}
