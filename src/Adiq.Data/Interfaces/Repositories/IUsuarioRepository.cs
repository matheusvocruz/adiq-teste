﻿using Adiq.Core.Interfaces;
using Adiq.Data.Entities;
using Adiq.Data.ValueObjetcs;
using System;
using System.Threading.Tasks;

namespace IMDb.Data.Interfaces.Repositorios
{
    public interface IUsuarioRepository : IRepository<Usuario>
    {
        Task<TokenResponse> Autenticar(string email, string senha);
        Task<TokenResponse> RefreshToken(Guid guid);
        string HashSenha(string value);
    }
}
